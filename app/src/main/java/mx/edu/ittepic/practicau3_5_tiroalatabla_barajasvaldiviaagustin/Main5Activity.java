package mx.edu.ittepic.practicau3_5_tiroalatabla_barajasvaldiviaagustin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;

public class Main5Activity extends AppCompatActivity {

    EditText cel;
    TextView celular, nombre, domicilio, antiguedad, sueldo, fechanac;
    Button eliminar;
    ConexionWeb conexionWeb;
    ProgressDialog dialogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);
        cel = new EditText(this);
        celular = findViewById(R.id.elicelular);
        nombre = findViewById(R.id.elinombre);
        domicilio = findViewById(R.id.elidomicilio);
        antiguedad = findViewById(R.id.eliantiguedad);
        sueldo = findViewById(R.id.elisalario);
        fechanac = findViewById(R.id.elifechanac);
        eliminar = findViewById(R.id.eliminar);

        eliminar.setEnabled(false);

        preguntaBusqueda();

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {eliminar();
            }
        });
    }

    private void eliminar() {
        try{
            conexionWeb = new ConexionWeb(this);
            conexionWeb.agregarVariable("celular", celular.getText().toString());
            URL direccion = new URL("https://tpdmagustin.000webhostapp.com/delete.php");
            dialogo = ProgressDialog.show(Main5Activity.this, "Atención", "Conectando con el servidor");
            conexionWeb.execute(direccion);
        }catch(MalformedURLException e){
            Toast.makeText(Main5Activity.this, "Error al formar URL", Toast.LENGTH_LONG).show();
        }

    }

    private void preguntaBusqueda() {
        AlertDialog.Builder buscar = new AlertDialog.Builder(this);
        buscar.setView(cel)
                .setTitle("Buscar")
                .setMessage("Introducir número de celular")
                .setPositiveButton("Buscar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            String valor = cel.getText().toString();
                            conexionWeb = new ConexionWeb(Main5Activity.this);
                            conexionWeb.agregarVariable("Celular", cel.getText().toString());
                            URL direccion = new URL("https://tpdmagustin.000webhostapp.com/read.php");
                            dialogo = ProgressDialog.show(Main5Activity.this, "Atención", "Conectando con el servidor");
                            conexionWeb.execute(direccion);
                            eliminar.setEnabled(true);
                        }catch(MalformedURLException e){
                            Toast.makeText(Main5Activity.this, "Error al formar URL", Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .setCancelable(false)
                .show();
    }

    public void procesarRespuesta(String respuesta) {
        dialogo.dismiss();
        if (respuesta.equals("ERROR_0")){
            Toast.makeText(this, "ERROR AL CONECTAR CON EL SERVIDOR", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        if (respuesta.equals("ERROR_1")){
            Toast.makeText(this, "ERROR AL CONECTAR CON LA BASE DE DATOS", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        if (respuesta.equals("ERROR_2")){
            Toast.makeText(this, "No se pudo eliminar", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        if (respuesta.equals("OK_1")){
            AlertDialog.Builder alert = new AlertDialog.Builder(Main5Activity.this);
            alert.setTitle("Correcto")
                    .setMessage("Eliminado de forma correcta\n¿Desea eliminar otro registro?")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent otra = new Intent(Main5Activity.this, Main5Activity.class);
                            startActivity(otra);
                            finish();
                        }
                    })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .setCancelable(false)
                    .show();
            return;
        }
        respuesta = respuesta.replace(":", "");
        String[] datos = respuesta.split("&");
        celular.setText(datos[0]);
        nombre.setText(datos[1]);
        domicilio.setText(datos[2]);
        antiguedad.setText(datos[3]);
        sueldo.setText(datos[4]);
        fechanac.setText(datos[5]);
    }
    public void cambiarMensaje(String s){
        dialogo.setMessage(s);
    }
}
