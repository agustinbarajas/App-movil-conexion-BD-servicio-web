package mx.edu.ittepic.practicau3_5_tiroalatabla_barajasvaldiviaagustin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity {

    Button insertar, consultar, actualizar, eliminar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        insertar = findViewById(R.id.insertar);
        consultar = findViewById(R.id.consultar);
        actualizar = findViewById(R.id.actualizar);
        eliminar = findViewById(R.id.eliminar);

        insertar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent create = new Intent(Main2Activity.this, MainActivity.class);
                startActivity(create);
            }
        });
        consultar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent read = new Intent(Main2Activity.this, Main3Activity.class);
                startActivity(read);
            }
        });
        actualizar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent update = new Intent(Main2Activity.this, Main4Activity.class);
                startActivity(update);
            }
        });
        eliminar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent delete = new Intent(Main2Activity.this, Main5Activity.class);
                startActivity(delete);
            }
        });

    }
}
