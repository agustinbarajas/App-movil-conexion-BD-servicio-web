package mx.edu.ittepic.practicau3_5_tiroalatabla_barajasvaldiviaagustin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;

public class Main4Activity extends AppCompatActivity {

    EditText cel, celular, nombre, domicilio, antiguedad, sueldo, fechanac;
    Button actualizar;
    ConexionWeb conexionWeb;
    ProgressDialog dialogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        cel = new EditText(this);
        celular = findViewById(R.id.actcelular);
        nombre = findViewById(R.id.actnombre);
        domicilio = findViewById(R.id.actdomicilio);
        antiguedad = findViewById(R.id.actantiguedad);
        sueldo = findViewById(R.id.actsalario);
        fechanac = findViewById(R.id.actfechanac);
        actualizar = findViewById(R.id.actualizar);

        celular.setEnabled(false);
        actualizar.setEnabled(false);

        preguntaBusqueda();

        actualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualizar();
            }
        });
    }

    private void actualizar() {
        try{
            conexionWeb = new ConexionWeb(this);
            conexionWeb.agregarVariable("celular", celular.getText().toString());
            conexionWeb.agregarVariable("nombre", nombre.getText().toString());
            conexionWeb.agregarVariable("domicilio", domicilio.getText().toString());
            conexionWeb.agregarVariable("antiguedad", antiguedad.getText().toString());
            conexionWeb.agregarVariable("sueldo", sueldo.getText().toString());
            conexionWeb.agregarVariable("fecha", fechanac.getText().toString());
            URL direccion = new URL("https://tpdmagustin.000webhostapp.com/update.php");
            dialogo = ProgressDialog.show(Main4Activity.this, "Atención", "Conectando con el servidor");
            conexionWeb.execute(direccion);
        }catch(MalformedURLException e){
            Toast.makeText(Main4Activity.this, "Error al formar URL", Toast.LENGTH_LONG).show();
        }

    }

    private void preguntaBusqueda() {
        AlertDialog.Builder buscar = new AlertDialog.Builder(this);
        buscar.setView(cel)
                .setTitle("Buscar")
                .setMessage("Introducir número de celular")
                .setPositiveButton("Buscar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            String valor = cel.getText().toString();
                            conexionWeb = new ConexionWeb(Main4Activity.this);
                            conexionWeb.agregarVariable("Celular", cel.getText().toString());
                            URL direccion = new URL("https://tpdmagustin.000webhostapp.com/read.php");
                            conexionWeb.execute(direccion);
                            dialogo = ProgressDialog.show(Main4Activity.this, "Atención", "Conectando con el servidor");
                            actualizar.setEnabled(true);
                        }catch(MalformedURLException e){
                            Toast.makeText(Main4Activity.this, "Error al formar URL", Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .setCancelable(false)
                .show();
    }

    public void procesarRespuesta(String respuesta) {
        dialogo.dismiss();
        if (respuesta.equals("ERROR_0")){
            Toast.makeText(this, "ERROR AL CONECTAR CON EL SERVIDOR", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        if (respuesta.equals("ERROR_1")){
            Toast.makeText(this, "ERROR AL CONECTAR CON LA BASE DE DATOS", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        if (respuesta.equals("ERROR_2")){
            Toast.makeText(this, "No se pudo actualizar", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        if (respuesta.equals("OK_1")){
            AlertDialog.Builder alert = new AlertDialog.Builder(Main4Activity.this);
            alert.setTitle("Correcto")
                    .setMessage("Actualización correcta\n¿Desea actualizar otro registro?")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent otra = new Intent(Main4Activity.this, Main4Activity.class);
                            startActivity(otra);
                            finish();
                        }
                    })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .setCancelable(false)
                    .show();
            return;
        }
        respuesta = respuesta.replace(":", "");
        String[] datos = respuesta.split("&");
        celular.setText(datos[0]);
        nombre.setText(datos[1]);
        domicilio.setText(datos[2]);
        antiguedad.setText(datos[3]);
        sueldo.setText(datos[4]);
        fechanac.setText(datos[5]);
    }
    public void cambiarMensaje(String s){
        dialogo.setMessage(s);
    }
}
