package mx.edu.ittepic.practicau3_5_tiroalatabla_barajasvaldiviaagustin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    EditText celular, nombre, domicilio, antiguedad, salario, fechanac;
    Button insertar;
    ConexionWeb conexionWeb;
    ProgressDialog dialogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        celular = findViewById(R.id.celular);
        nombre = findViewById(R.id.nombre);
        domicilio = findViewById(R.id.domicilio);
        antiguedad = findViewById(R.id.antiguedad);
        salario = findViewById(R.id.salario);
        fechanac = findViewById(R.id.fechanac);
        insertar = findViewById(R.id.insertar);
        setTitle("Inserción");

        insertar.setOnClickListener(new View.OnClickListener(){
           @Override
            public void onClick(View v){
               try{
                   conexionWeb = new ConexionWeb(MainActivity.this);
                   conexionWeb.agregarVariable("celular", celular.getText().toString());
                   conexionWeb.agregarVariable("nombre", nombre.getText().toString());
                   conexionWeb.agregarVariable("domicilio", domicilio.getText().toString());
                   conexionWeb.agregarVariable("antiguedad", antiguedad.getText().toString());
                   conexionWeb.agregarVariable("sueldo", salario.getText().toString());
                   conexionWeb.agregarVariable("fecha", fechanac.getText().toString());
                   URL direccion = new URL("https://tpdmagustin.000webhostapp.com/create.php");
                   dialogo = ProgressDialog.show(MainActivity.this, "Atención", "Conectando con el servidor");
                   conexionWeb.execute(direccion);
               }catch (MalformedURLException e){
                   Toast.makeText(MainActivity.this, "ERROR, url mal formada", Toast.LENGTH_LONG).show();
               }
           }
        });
    }
    public void procesarRespuesta(String respuesta){
        if (respuesta.equals("ERROR_0")){
            Toast.makeText(this, "ERROR AL CONECTAR CON EL SERVIDOR", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        if (respuesta.equals("ERROR_1")){
            Toast.makeText(this, "ERROR AL CONECTAR CON LA BASE DE DATOS", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        if (respuesta.equals("ERROR_2")){
            Toast.makeText(this, "No se pudo insertar", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        if (respuesta.equals("OK_1")){
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Correcto")
                    .setMessage("Inserción correcta\n¿Desea realizar otra inserción?")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            celular.setText("");
                            nombre.setText("");
                            domicilio.setText("");
                            antiguedad.setText("");
                            salario.setText("");
                            fechanac.setText("");
                        }
                    })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }
    public void cambiarMensaje(String s){
        dialogo.setMessage(s);
    }
}
