package mx.edu.ittepic.practicau3_5_tiroalatabla_barajasvaldiviaagustin;

import android.os.AsyncTask;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by agustin on 10/04/18.
 */

public class ConexionWeb extends AsyncTask<URL, String, String> {
    List<String[]> variables;
    Main3Activity puntero3;
    MainActivity puntero;
    Main4Activity puntero4;
    Main5Activity puntero5;

    public ConexionWeb(MainActivity p){
        variables = new ArrayList<>();
        puntero = p;
    }
    public ConexionWeb(Main3Activity p){
        variables = new ArrayList<>();
        puntero3 = p;
    }
    public ConexionWeb(Main4Activity p){
        variables = new ArrayList<>();
        puntero4 = p;
    }
    public ConexionWeb(Main5Activity p){
        variables = new ArrayList<>();
        puntero5 = p;
    }
    public void agregarVariable(String clave, String valor){
        variables.add(new String[]{clave, valor});
    }
    @Override
    protected String doInBackground(URL ... urls){
        String post = "";
        String respuesta = "";

        for (String[] var: variables){
            try{
                post += var[0] + "=" + URLEncoder.encode(var[1], "UTF-8") + " ";
            }catch(Exception e){
                return "ERROR_404_0";
            }
        }
        post = post.trim();
        post = post.replace(" ", "&");
        HttpURLConnection conexion = null;
        try{
            publishProgress("Intentando conectar");
            conexion = (HttpURLConnection)urls[0].openConnection();
            conexion.setDoInput(true);
            conexion.setFixedLengthStreamingMode(post.length());
            conexion.setRequestMethod("POST");
            conexion.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            OutputStream flujoSalida = new BufferedOutputStream(conexion.getOutputStream());
            flujoSalida.write(post.getBytes());
            flujoSalida.flush();
            flujoSalida.close();

            if (conexion.getResponseCode() == 200){
                InputStreamReader input = new InputStreamReader(conexion.getInputStream());
                BufferedReader flujoentrada = new BufferedReader(input);
                String linea = "";
                do {
                    linea = flujoentrada.readLine();
                    if (linea != null)
                        respuesta += linea;
                }while(linea != null);
                flujoentrada.close();
            }else
                return "ERROR_404_1";
        }catch (UnknownHostException e){
            return "ERROR_404_2";
        }catch(IOException e){
            return "ERROR_404_3";
        }finally {
            if (conexion != null)
                conexion.disconnect();
        }
        return respuesta;
    }
    protected void onProgressUpdate(String... r){
        if (puntero != null) {
            puntero.cambiarMensaje(r[0]);
        }
        if (puntero3 != null) {
            puntero3.cambiarMensaje(r[0]);
        }
        if (puntero4 != null) {
            puntero4.cambiarMensaje(r[0]);
        }
        if (puntero5 != null) {
            puntero5.cambiarMensaje(r[0]);
        }
    }

    @Override
    protected void onPostExecute(String respuesta){
        if (puntero != null) {
            puntero.procesarRespuesta(respuesta);
        }
        if (puntero3 != null) {
            puntero3.procesarRespuesta(respuesta);
        }
        if (puntero4 != null) {
            puntero4.procesarRespuesta(respuesta);
        }
        if (puntero5 != null) {
            puntero5.procesarRespuesta(respuesta);
        }
    }
}
