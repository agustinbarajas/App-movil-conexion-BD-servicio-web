package mx.edu.ittepic.practicau3_5_tiroalatabla_barajasvaldiviaagustin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Main3Activity extends AppCompatActivity {

    Spinner criterio;
    EditText search;
    ListView consultas;
    ConexionWeb conexionWeb;
    List<String[]> datosreg;
    ProgressDialog dialogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        setTitle("Consulta");

        consultas = findViewById(R.id.res_consulta);
        conexionWeb = new ConexionWeb(this);

        preguntaBusqueda();


    }

    private void preguntaBusqueda() {
        View layout = getLayoutInflater().inflate(R.layout.layout_busqueda, null);

        criterio = layout.findViewById(R.id.criterio);
        search = layout.findViewById(R.id.search);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Búsqueda")
                .setView(layout)
                .setPositiveButton("Buscar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String crit_bus = criterio.getSelectedItem().toString();
                        crit_bus = crit_bus.replace(" ", "");
                        String dato_bus = search.getText().toString();
                        if (!dato_bus.isEmpty()){
                            try {
                                conexionWeb.agregarVariable(crit_bus, dato_bus);
                                URL direccion = new URL("https://tpdmagustin.000webhostapp.com/read.php");
                                dialogo = ProgressDialog.show(Main3Activity.this, "Atención", "Conectando con el servidor");
                                conexionWeb.execute(direccion);
                            }catch(MalformedURLException e){
                                Toast.makeText(getApplicationContext(), "ERROR, URL mal formada", Toast.LENGTH_LONG).show();
                            }
                        }else{
                            dialog.dismiss();
                            finish();
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .setCancelable(false)
                .show();
    }

    public void procesarRespuesta(String respuesta){
        dialogo.dismiss();
        if (respuesta.equals("ERROR_0")){
            Toast.makeText(this, "ERROR AL CONECTAR CON EL SERVIDOR", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        if (respuesta.equals("ERROR_1")){
            Toast.makeText(this, "ERROR AL CONECTAR CON LA BASE DE DATOS", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        if (respuesta.equals("ERROR_2")){
            Toast.makeText(this, "No se encontraron coincidencias", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        String[] registros = respuesta.split(":");
        String[] encabezados = new String[registros.length];
        datosreg = new ArrayList<>();
        for (int i=0; i<registros.length; i++){
            datosreg.add(registros[i].split("&"));
            encabezados[i] = datosreg.get(i)[1];
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezados);
        consultas.setAdapter(adapter);
        consultas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                android.app.AlertDialog.Builder msj = new android.app.AlertDialog.Builder(Main3Activity.this);
                String datos = "Celular: {CELULAR}\n" +
                        "Nombre : {NOMBRE}\n" +
                        "Domicilio : {DOMICILIO}\n" +
                        "Antigüedad : {ANTIGUEDAD}\n" +
                        "Sueldo : {SUELDO}\n" +
                        "Fecha de nacimento : {FECHANACIMIENTO}";
                datos = datos.replace("{CELULAR}", datosreg.get(position)[0]);
                datos = datos.replace("{NOMBRE}", datosreg.get(position)[1]);
                datos = datos.replace("{DOMICILIO}", datosreg.get(position)[2]);
                datos = datos.replace("{ANTIGUEDAD}", datosreg.get(position)[3]);
                datos = datos.replace("{SUELDO}", datosreg.get(position)[4]);
                datos = datos.replace("{FECHANACIMIENTO}", datosreg.get(position)[5]);

                msj.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                msj.setTitle("Datos empleado")
                        .setMessage(datos)
                        .setCancelable(false)
                        .show();
            }
        });
    }
    public void cambiarMensaje(String s) {
        dialogo.setMessage(s);
    }
}
